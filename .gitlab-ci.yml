# This file is a template, and might need editing before it works on your project.
# see https://docs.gitlab.com/ee/ci/yaml/README.html for all available options

variables:
  DEFAULT_IMAGE: debian:stable
  APT_CACHE_DIR: $CI_PROJECT_DIR/.cache/apt
  NPM_CONFIG_CACHE: $CI_PROJECT_DIR/.cache/npm

  #
  # We have code in here for releasing on both GS and S3, control it through variables
  # on GitLab, do not change it in this file.
  #
  GCLOUD_SERVICE_ACCOUNT: /dev/null
  GCLOUD_RELEASE_DEPLOY_PATH: gs://download.gridtracker.org/release/$CI_COMMIT_TAG
  GCLOUD_TESTING_DEPLOY_PATH: gs://download.gridtracker.org/testing/$CI_COMMIT_TAG
  NR0Q_SERVER_ADDRESS: ""
  NR0Q_SERVER_PASSWORD: ""

# nothing in this file ill be allowed to run automatically except for:
# 1. merge requests
# 2. manual tagging
# 3. committing to the default branch
include:
  - template: "Workflows/MergeRequest-Pipelines.gitlab-ci.yml"

stages:
  - build
  - test
  - package
  - deploy

default:
  image: ${DEFAULT_IMAGE}
  interruptible: true

# just do a quick syntax check job, we don't need to "build" anything here other than the
# outer dev environment for gridtracker
npm_test:
  stage: test
  image: node:latest
  script:
    - npm install
    - npm test

# test2:
#   stage: test
#   script:
#     - echo "Do another parallel test here"
#     - echo "For example run a lint test"

# package binaries and create build artifacts that may be used in later stages
win-packaging:
  stage: package
  rules:
    # only do this with a manual tag starting with v or test_
    - if: '$CI_COMMIT_TAG =~ /^(v|test_).*/'
  artifacts:
    paths:
      - artifacts/
    name: '$CI_COMMIT_REF_SLUG'
  cache:
    paths:
      - .cache/
      - node_modules/
    key:
      files:
        - package.json
        - package.nw/package.json
  script:
    - |
      mkdir -p $APT_CACHE_DIR $NPM_CONFIG_CACHE
      mkdir -p artifacts
      dpkg --add-architecture i386
    - |
      echo -e "\e[0Ksection_start:`date +%s`:apt_get[collapsed=true]\r\e[0KGetting Build Dependencies"
      apt-get -qq update
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y npm wine wine32
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y build-essential devscripts
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y zip nsis nsis nsis-pluginapi wget
      wget https://nsis.sourceforge.io/mediawiki/images/4/47/Registry.zip
      unzip Registry.zip
      mv Desktop/Plugin/registry.dll /usr/share/nsis/Plugins/x86-unicode/
      mv Desktop/Include/Registry.nsh /usr/share/nsis/Include/
      rm -Rf Desktop PocketPC Source
      echo -e "\e[0Ksection_end:`date +%s`:apt_get\r\e[0K"
    - test `node version.js` = `dpkg-parsechangelog -S version` || (echo "package.nw/package.json and debian/changelog version mismatch"; exit 1)
    # Making our Windows packages
    - |
      echo -e "\e[0Ksection_start:`date +%s`:native_build\r\e[0KBuilding native packages"
      npm install --prefer-offline
      npm run dist-win
      for dir in dist/*-win-* ; do
          if [ -d $dir ] ; then
              rm $dir/locales/*.info
              mkdir $dir/package.nw
              for file in package.nw/* ; do
                mv $dir/`basename $file` $dir/package.nw
              done
          elif [  -f $dir ] && [[ "$dir" == *"win-x86-Setup.exe"* ]] ; then
            echo "deleting broken installer $dir"
            rm $dir
          fi
      done
      sed "s#GridTracker-\${VERSION}-win-x86/#`pwd`/dist/GridTracker-\${VERSION}-win-x86/#g" windows/setup.nsi.tmpl > windows/setup.nsi.tmp.1
      sed "s#GridTracker-Installer.#`pwd`/dist/GridTracker-Installer.#g" windows/setup.nsi.tmp.1 > windows/setup.nsi.tmp.2
      sed "s#define VERSION <placeholder#define VERSION `node version.js`#g" windows/setup.nsi.tmp.2 > windows/setup.nsi

      makensis windows/setup.nsi
      # clean up generated files
      rm windows/setup.nsi
      rm windows/setup.nsi.tmp.1
      rm windows/setup.nsi.tmp.2
      (cd dist ; mv GridTracker-Installer.*.exe ../artifacts)
      echo -e "\e[0Ksection_end:`date +%s`:native_build\e[0K"


intel-packaging:
  stage: package
  rules:
    # only do this with a manual tag starting with v or test_
    - if: '$CI_COMMIT_TAG =~ /^(v|test_).*/'
  artifacts:
    paths:
      - artifacts/
    name: '$CI_COMMIT_REF_SLUG'
  cache:
    paths:
      - .cache/
      - node_modules/
    key:
      files:
        - package.json
        - package.nw/package.json
  script:
    - |
      mkdir -p $APT_CACHE_DIR $NPM_CONFIG_CACHE
      mkdir -p artifacts/rpm
      dpkg --add-architecture i386
    - |
      echo -e "\e[0Ksection_start:`date +%s`:apt_get[collapsed=true]\r\e[0KGetting Build Dependencies"
      apt-get -qq update
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y npm wine wine32
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y build-essential devscripts
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y rpm zip wget
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" build-dep .
      echo -e "\e[0Ksection_end:`date +%s`:apt_get\r\e[0K"
    - test `node version.js` = `dpkg-parsechangelog -S version` || (echo "package.nw/package.json and debian/changelog version mismatch"; exit 1)
    # Making our MacOS and distro agnostic Linux packages
    - |
      echo -e "\e[0Ksection_start:`date +%s`:native_build\r\e[0KBuilding native packages"
      npm install --prefer-offline
      npm run dist-nix
      chmod 755 dist/*-linux-*/GridTracker dist/*-linux-*/lib dist/*-linux-*/locales dist/*-linux-*/swiftshader/
      for dir in dist/*-linux-* ; do
        if [ -d $dir ] ; then
          cp package.nw/img/gridtracker.png $dir/
          rm $dir/locales/*.info
          tar -C dist -czf ${dir}.tar.gz `basename $dir`
        fi
      done
      (cd dist ; mv *-mac-x64.zip *.tar.gz ../artifacts)
      echo -e "\e[0Ksection_end:`date +%s`:native_build\e[0K"
    # This section handles making RPMs for Fedora/CentOS/RHEL
    - |
      echo -e "\e[0Ksection_start:`date +%s`:rpm_build\r\e[0KBuilding RPM packages"
      # mkdir -p $HOME/rpmbuild/SOURCES
      # cp artifacts/*.tar.gz $HOME/rpmbuild/SOURCES/
      setarch i386 rpmbuild -D "version `node ./version.js`" --build-in-place -bb gridtracker.i386.spec
      setarch x86_64 rpmbuild -D "version `node ./version.js`" --build-in-place -bb gridtracker.x86_64.spec
      mv $HOME/rpmbuild/RPMS/i386/*.rpm artifacts/rpm/
      mv $HOME/rpmbuild/RPMS/x86_64/*.rpm artifacts/rpm/
      echo -e "\e[0Ksection_end:`date +%s`:rpm_build\r\e[0K"
    # This section does debian DEBs
    #- |
    #  echo -e "\e[0Ksection_start:`date +%s`:debian_build\r\e[0KBuilding Debian packages"
    #  dpkg-buildpackage -uc -us
    #  mkdir artifacts/debian
    #  mv ../*.{deb,dsc,buildinfo,tar.xz,changes} artifacts/debian/
    #  echo -e "\e[0Ksection_end:`date +%s`:debian_build\r\e[0K"
    - |
      echo "we made packages!"
      ls -laR artifacts

arm-packaging:
  stage: package
  rules:
    # only do this with a manual tag starting with v or test_
    - if: '$CI_COMMIT_TAG =~ /^(v|test_).*/'
  artifacts:
    paths:
      - artifacts/
    name: '$CI_COMMIT_REF_SLUG'
  cache:
    paths:
      - .cache/
      - node_modules/
    key:
      files:
        - package.json
        - package.nw/package.json
  script:
    - |
      mkdir -p $APT_CACHE_DIR $NPM_CONFIG_CACHE
      mkdir -p artifacts/rpm
      dpkg --add-architecture armhf
    - |
      echo -e "\e[0Ksection_start:`date +%s`:apt_get[collapsed=true]\r\e[0KGetting Build Dependencies"
      apt-get -qq update
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y npm
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y build-essential devscripts
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" install -y rpm zip wget
      apt-get -qq -o dir::cache::archives="$APT_CACHE_DIR" build-dep .
      echo -e "\e[0Ksection_end:`date +%s`:apt_get\r\e[0K"
    - test `node version.js` = `dpkg-parsechangelog -S version` || (echo "package.nw/package.json and debian/changelog version mismatch"; exit 1)
    # This is for ARM arch build
    - |
      echo -e "\e[0Ksection_start:`date +%s`:arm_build\r\e[0KBuilding ARM packages"
      wget https://github.com/LeonardLaszlo/nw.js-armv7-binaries/releases/download/nw49_2020-10-26/nw49_2020-10-26.tar.gz
      tar -xf nw49_2020-10-26.tar.gz
      tar -xf usr/docker/dist/nwjs-chromium-ffmpeg-branding/nwjs-v0.49.2-linux-arm.tar.gz
      rm nwjs-v0.49.2-linux-arm/locales/*.info
      mv nwjs-v0.49.2-linux-arm/nw nwjs-v0.49.2-linux-arm/GridTracker
      ls
      version=`node ./version.js`
      mkdir GridTracker-$version-linux-arm
      cp -r nwjs-v0.49.2-linux-arm/* GridTracker-$version-linux-arm/
      cp -r arm/* GridTracker-$version-linux-arm/
      cp -r package.nw GridTracker-$version-linux-arm/
      cp LICENSE GridTracker-$version-linux-arm/
      tar -czf GridTracker-$version-linux-arm.tar.gz GridTracker-$version-linux-arm
      mv GridTracker-$version-linux-arm.tar.gz artifacts/
    # This section handles making RPMs for Fedora/CentOS/RHEL
    #- |
      #  echo -e "\e[0Ksection_start:`date +%s`:rpm_build\r\e[0KBuilding RPM packages"
      # mkdir -p $HOME/rpmbuild/SOURCES
      # cp artifacts/*.tar.gz $HOME/rpmbuild/SOURCES/
      #setarch --list
      #setarch armhf rpmbuild -D "version `node ./version.js`" --build-in-place -bb gridtracker.arm.spec
      #mv $HOME/rpmbuild/RPMS/armv7l/*.rpm artifacts/rpm/
      #echo -e "\e[0Ksection_end:`date +%s`:rpm_build\r\e[0K"
    # This section does debian DEBs
    #- |
    #  echo -e "\e[0Ksection_start:`date +%s`:debian_build\r\e[0KBuilding Debian packages"
    #  dpkg-buildpackage -uc -us
    #  mkdir artifacts/debian
    #  mv ../*.{deb,dsc,buildinfo,tar.xz,changes} artifacts/debian/
    #  echo -e "\e[0Ksection_end:`date +%s`:debian_build\r\e[0K"
    - |
      echo "we made packages!"
      ls -laR artifacts

# copy test assets to NR0Q's server
nr0q_upload_testing:
  stage: deploy
  image: alpine
  rules:
    - if: '$CI_COMMIT_TAG =~ /^(test_).*/ && $NR0Q_SERVER_KEY != ""'
  before_script:
    - |
      apk add openssh-client
      eval $(ssh-agent -s)
      echo "$NR0Q_SERVER_KEY" | tr -d '\r' | ssh-add -
      mkdir -p ~/.ssh; chmod 700 ~/.ssh
  script:
    - |
      cd artifacts
      for FILE in *.*
      do
        echo "Calculating MD5 Hash for $FILE"
        md5sum $FILE > $FILE.md5
      done
      for FILE in rpm/*.rpm
      do
        echo "Calculating MD5 Hash for $FILE"
        md5sum $FILE > $FILE.md5
      done

      ssh -o StrictHostKeyChecking=no gridtracker@$NR0Q_SERVER_ADDRESS "mkdir ~/$CI_COMMIT_TAG" && scp -o StrictHostKeyChecking=no -r ./* gridtracker@$NR0Q_SERVER_ADDRESS:~/$CI_COMMIT_TAG/ && ssh -o StrictHostKeyChecking=no gridtracker@$NR0Q_SERVER_ADDRESS "rm latest_test && ln -s $CI_COMMIT_TAG latest_test"

# copy release assets to NR0Q's server
nr0q_upload_release:
  stage: deploy
  image: alpine
  rules:
    - if: '$CI_COMMIT_TAG =~ /^(v).*/ && $NR0Q_SERVER_KEY != ""'
  before_script:
    - |
      apk add openssh-client
      eval $(ssh-agent -s)
      echo "$NR0Q_SERVER_KEY" | tr -d '\r' | ssh-add -
      mkdir -p ~/.ssh; chmod 700 ~/.ssh
  script:
    - |
      cd artifacts
      for FILE in *.*
      do
        echo "Calculating MD5 Hash for $FILE"
        md5sum $FILE > $FILE.md5
      done
      for FILE in rpm/*.rpm
      do
        echo "Calculating MD5 Hash for $FILE"
        md5sum $FILE > $FILE.md5
      done
      ssh -o StrictHostKeyChecking=no gridtracker@$NR0Q_SERVER_ADDRESS "mkdir ~/$CI_COMMIT_TAG" && scp -o StrictHostKeyChecking=no -r ./* gridtracker@$NR0Q_SERVER_ADDRESS:~/$CI_COMMIT_TAG/

# this only creates a "source code release" -- gitlab doesn't specify binaries
# except as links to external storage, which is suboptimal for now

source_release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: '$CI_COMMIT_TAG =~ /^(v).*/'
  release:
    tag_name: $CI_COMMIT_TAG
    name: GridTracker $CI_COMMIT_TAG
    description: GridTracker release $CI_COMMIT_TAG
    ref: '$CI_COMMIT_SHA'
    assets:
      links:
        - name: Release Packages for $CI_COMMIT_TAG
          url: https://fleetwood.mchambersradio.com/gridtracker/$CI_COMMIT_TAG/
          external: true
          link_type: package
  script:
    - echo 'Release for $CI_COMMIT_TAG'
